﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IgromaniaClientApplication
{
    class ComputerManager
    {
        private static ComputerManager instance;
        
        public static ComputerManager getInstance()
        {
            if (instance == null)
            {
                instance = new ComputerManager();
            }

            return instance;
        }

        public void RegisterOnServer(String serverAddr)
        {
            SocketWorker.getInstance().Connect(serverAddr);
        }

        [DllImport("user32.dll")]
        private static extern bool BlockInput(bool block);

        public static void FreezeMouse()
        {
            BlockInput(true);
        }

        public static void ThawMouse()
        {
            BlockInput(false);
        }

    }
}
