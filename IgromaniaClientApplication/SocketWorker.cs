﻿using IgromaniaClientApplication.models;
using Quobject.SocketIoClientDotNet.Client;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IgromaniaClientApplication
{
    class SocketWorker
    {
        private static SocketWorker instance;
        private Socket _socket;

        public BehaviorSubject<SocketConnectionState> connectionState = new BehaviorSubject<SocketConnectionState>(null);

        public Socket Socket
        {
            get
            {
                return _socket;
            }
        }
        
        public static SocketWorker getInstance()
        {
            if (instance == null)
            {
                instance = new SocketWorker();
            }

            return instance;
        }

        private void BindListeners()
        {
            Socket.On(Socket.EVENT_CONNECT, () =>
            {
                connectionState.OnNext(new SocketConnectionState
                {
                    Color = Color.Green,
                    Label = "Подключено"
                });

                Socket.On("message", (message) =>
                {
                    Console.WriteLine("Got new message");
                    Console.WriteLine(message);
                });

                Socket.Emit("identity");
            });

            Socket.On(Socket.EVENT_DISCONNECT, () =>
            {
                connectionState.OnNext(new SocketConnectionState
                {
                    Color = Color.Gray,
                    Label = "Отключено"
                });
            });

            Socket.On(Socket.EVENT_CONNECT_ERROR, () =>
            {
                connectionState.OnNext(new SocketConnectionState
                {
                    Color = Color.Red,
                    Label = "Ошибка"
                });
            });

            Socket.On(Socket.EVENT_CONNECT_TIMEOUT, () =>
            {
                connectionState.OnNext(new SocketConnectionState
                {
                    Color = Color.Gray,
                    Label = "Таймаут"
                });
            });

            Socket.On(Socket.EVENT_RECONNECT_ATTEMPT, () =>
            {
                connectionState.OnNext(new SocketConnectionState
                {
                    Color = Color.DarkGoldenrod,
                    Label = "Переподключение..."
                });
            });

            
        }

        public void Connect(String dsn)
        {
            if (Socket == null)
            { 
                _socket = IO.Socket(dsn);
            }

            _socket.Open();
        }

        public void Connect(String dsn, IO.Options opts)
        {
            Console.WriteLine(Socket);
           
            if (Socket == null)
            {
                _socket = IO.Socket(dsn, opts);
                BindListeners();
            }

            _socket.Connect();
        }

        public void Disconnect()
        {
            if (Socket == null) return;

            _socket.Disconnect();
            _socket.Close();
            _socket = null;
        }
    }
}
