﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IgromaniaClientApplication.models
{
    class SocketConnectionState
    {
        public string Label;
        public Color Color;

        public SocketConnectionState() { }
    }
}
