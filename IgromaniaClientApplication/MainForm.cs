﻿using IgromaniaClientApplication.models;
using Quobject.SocketIoClientDotNet.Client;
using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace IgromaniaClientApplication
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            Init();
        }

        private void Init()
        {
            LoadSettings();
            ConnectToServer();
        }

        private void LoadSettings()
        {
            addressTextBox.Text = Properties.Settings.Default["ServerAddress"].ToString();
        }

        private void ConnectToServer()
        {
            String serverAddr = Properties.Settings.Default["ServerAddress"].ToString();
            if (String.IsNullOrEmpty(serverAddr))
            {
                return;
            }

            SocketWorker sw = SocketWorker.getInstance();

            serverAddr = String.Format("http://{0}:{1}", serverAddr, 3334);

            var options = new IO.Options
            {
                Timeout = 3000,
                AutoConnect = false,
                //ForceNew = true,
                //IgnoreServerCertificateValidation = true
            };

            sw.Connect(serverAddr, options);
            sw.connectionState.Subscribe((SocketConnectionState state) =>
            {
                if (state == null) return;

                statusLabel.BeginInvoke((MethodInvoker)(() => statusLabel.Text = state.Label));
                statusLabel.BeginInvoke((MethodInvoker)(() => statusLabel.ForeColor = state.Color));
            });
        }


        private void saveSettingsButton_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.ServerAddress = addressTextBox.Text;
            Properties.Settings.Default.Save();
            SocketWorker.getInstance().Disconnect();

            new Thread(() =>
            {
                SocketWorker.getInstance().connectionState.OnNext(new SocketConnectionState
                {
                    Color = Color.Gray,
                    Label = "Ожидание переподключения..."
                });

                Thread.Sleep(2000);
                ConnectToServer();
            }).Start();
        }
    }
}
